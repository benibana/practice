# コンテンツモデルについて


### コンテンツモデル→「HTML要素にどのようなコンテンツを含めてもよいか」

``` html
<ul>
  <li></li>
  <li></li>
</ul>
```

たとえば、

「`<ul>` 要素には、直下に`<li>`以外を含めてはいけない」というルールを詳細に定義したのがコンテンツモデル。

正確には、「0個以上の`li`要素」と記載されている。

<http://www.w3.org/TR/html5/grouping-content.html#the-ul-element>

---

### カテゴリで分類される
カテゴリとは、コンテンツモデルを目的に応じてグルーピングするためのもの。

ひとつの要素がひとつのカテゴリに属しているとは限らない。ひとつの要素が複数のカテゴリに属することもある。（どのカテゴリにも属さない要素のもある）



|メタデータコンテンツの要素|
|:--|
|`<base> <command> <link> <meta> <noscript> <script> <style> <title>`|

`<head>`要素内に含まれるようなコンテンツ


|フローコンテンツの要素|
|:--|
|`<article> <aside> <audio> <b> <bdo> <blockquote> <brv> <button> `|
|`<canvas> <cite> <code> <command> <datalist> <del> <details> <dfn>`|
|`<div> <dl> <em> <embed> <fieldset> <figure> <footer> <form> <h1>～<h6>`|
|`<header> <hr> <i> <iframe> <img> <input> <ins> <kbd> <keygen> <label>` |
|`<map> <mark> <math> <menu> <meter> <nav> <noscript> <object> <ol>`|
|`<output> <p> <pre> <progress> <q> <ruby> <samp> <vscript><section>`|
|`<select> <small> <span> <strong> <sub> <sup> <svg> <table>`|
|`<textarea> <time> <ul> <var> <video> <wbr> テキスト`|

`<body>`要素内で使用するほとんどのコンテンツ

|セクショニングコンテンツの要素|
|:--|
|`<article> <aside> <nav> <section>`    |

章や節など、見出しとその概要を伴うセクションを定義するコンテンツ


|ヘッディングコンテンツの要素|
|:--|
|`<h1>～<h6>`|

|フレージングコンテンツの要素|
|:--|
|`<abbr> <audio> <b> <bdo> <br> <button> <canvas> <cite> <code> <command>`|
|`<datalist> <dfn> <em> <embed> <i> <iframe> <mark> <img> <input> <kbd>`|
|`<keygen> <label> <mark> <math> <meter> <noscript> <object> <output>`
|`<progress> <q> <ruby> <samp> <script> <select> <small> <span> <strong>`|
|`<sub> <svg> <textarea> <time> <var> <video> <wbr>、テキスト`|

テキストとテキストの文節・語句などの要素を含む。
特定の要素を含む場合、次の要素もフレージングコンテンツに属する。

* `<a>` フレージングコンテンツだけを含む場合
* `<area>` map要素の子孫である場合
* `<del> <ins> <map>` フレージングコンテンツだけを含む場合
* `<link> <meta>` itemprop属性が存在する場合

|埋め込みコンテンツの要素|
|:--|
|`<audio> <canvas> <embed> <iframe> <img> <math> <object> <svg> <video>`|

文書にほかのリソースを取り込んだり、HTMLではないほかの言語を挿入するコンテンツ。

|インタラクティブコンテンツの要素|
|:--|
|`<a> <button> <details> <embed> <iframe> <keygen> <label> <select> <textarea>`|

ユーザとなにかしらやりとりできるコンテンツを含む。特定の条件が満たされている場合、次の要素もインタラクティブコンテンツに属する。

* `<audio> <video>`　controls属性が存在する場合
* `<img> <object>` usemap属性が存在する場合
* `<input>` type属性の値がhiddenではない場合
* `<menu>` type属性の値がtoolbarではない場合

---

#### トランスペアレントコンテンツモデル

要素が「トランスペアレント」に属する場合、その親のコンテンツモデルを継承する。

``` html
<div>
  <a href="{url}"><p>テキスト</p></a>
</div>
```

`<a>`要素はトランスペアレントで、この場合、`<a>`は`<div>`要素のコンテンツモデルを継承するので、フローコンテンツと同等ということになる。

`<ins> <del> <map>`などもトランスペアレントに属する要素。

---

#### セクションとアウトライン

暗黙的なセクショニング：見出し要素が現れた時点をセクションの始まりとみなすアウトラインの定義方法。古い。

明示的なセクショニング：見出しのランクよりもセクショニング要素(`<article> <aside> <nav> <section>`)を重要視する。見出しランクの順序を厳密に守る必要がなくなった。`<h1>`を汎用的な見出しとして用いることができる。

---

#### どのセクション具要素が適切か迷ったとき

参照：
<http://html5doctor.com/resources/#flowchart>

nav要素：主要なナビゲーションを表すセクショニング。（グローバルナビゲーション、ローカルナビゲーション、パンくずリスト、ページネーションなど。）

article要素：自己完結したセクショニング。「仮にRSSフィードで読み込まれたとして、独立したコンテンツとして成り立っているか？」を考える。（ニュース記事やブログコメントのように、単独で再配布できるコンテンツ。）

aside要素：メインコンテンツと関連性のうすいコンテンツ。これがなくてもページが成り立つ。

section要素：セクショニング要素の中で一番汎用的。ほかの3つの要素のどれにもあてはまらないもの。ただし、グルーピングするコンテンツは関連性を持っている必要がある。

※みため（スタイル）を変えるだけならdivを使うのが妥当。




---

memo

`<bdo>`は文字表記の基本方向を指定するタグです。 日本ではあまり使用する機会のないタグですが、ヘブライ語のように右から左に記述する言語を表記する際に使用します。

`<kbd>`タグは、ユーザーが入力する内容であることを示す際に使用します。 主にキーボード入力ですが、音声コマンドなどの他の方法による入力を表すために使用されることもあるでしょう。
