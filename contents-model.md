 #コンテンツモデルについて

 ####「HTML要素にどのようなコンテンツを含めてもよいか」

 ```HTML
 <ul>
    <li></li>
    <li></li>
 </ul>
 ```

たとえば、

「`<ul>` 要素には、直下に`<li>`以外を含めてはいけない」というルールを詳細に定義したのがコンテンツモデル。

正確には、「0個以上の`li`要素」と記載されている。

<http://www.w3.org/TR/html5/grouping-content.html#the-ul-element>


####カテゴリで分類される
カテゴリとは、コンテンツモデルを目的に応じてグルーピングするためのもの。

ひとつの要素がひとつのカテゴリに属しているとは限らない。ひとつの要素が複数のカテゴリに属することもある。（どのカテゴリにも属さない要素のもある）



|メタデータコンテンツの要素|
|:--|
|`<base> <command> <link> <meta> <noscript> <script> <style> <title>`    |
`<head>`要素内に含まれるようなコンテンツ


|フローコンテンツの要素|
|:--|
|`<article> <aside> <audio> <b> <bdo> <blockquote> <brv> <button> `<br> `<canvas> <cite> <code> <command> <datalist> <del> <details> <dfn> `<br>`<div> <dl> <em> <embed> <fieldset> <figure> <footer> <form> <h1>～<h6>` <br> `<header> <hr> <i> <iframe> <img> <input> <ins> <kbd> <keygen> <label> `<br>`<map> <mark> <math> <menu> <meter> <nav> <noscript> <object> <ol>`<br>`<output> <p> <pre> <progress> <q> <ruby> <samp> <vscript> <section> `<br>`<select> <small> <span> <strong> <sub> <sup> <svg> <table> <textarea>`<br>`<time> <ul> <var> <video> <wbr> テキスト`|

`<body>`要素内で使用するほとんどのコンテンツ

|セクショニングコンテンツの要素|
|:--|
|`<article> <aside> <nav> <section>`    |
`<head>`章や節など、見出しとその概要を伴うセクションを定義するコンテンツ


|ヘッディングコンテンツ|
|:--|
|`<h1>～<h6>`    |

|フレージングコンテンツ|
|:--|
|``    |
